package cat.itb.itbcalendar.presentation.itbcalendar_data_classes.repository;

import cat.itb.itbcalendar.presentation.itbcalendar_data_classes.repository.data.MpData;
import cat.itb.itbcalendar.presentation.itbcalendar_data_classes.repository.data.SessionData;

/**
 * MpRepository Factory
 */
public class MpRepositoryFactory {
    private static MpRepository mpRepository;

    /**
     * @return singleton instance of MpRepository
     */
    public static MpRepository getInstance() {
        if(mpRepository==null){
            mpRepository = new MpRepository(new SessionData(new MpData()));
        }
        return mpRepository;
    }
}
