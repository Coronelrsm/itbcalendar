package cat.itb.itbcalendar.presentation.itbcalendar_data_classes.domain;

public class Mp {
    int number;
    String name;
    String teacher;
    String description;

    public Mp(int number, String name, String teacher, String description) {
        this.number = number;
        this.name = name;
        this.teacher = teacher;
        this.description = description;
    }

    public int getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public String getTeacher() {
        return teacher;
    }

    public String getDescription() {
        return description;
    }

    public String toString(){
        return "MP"+getNumber()+" - "+getName();
    }
}
