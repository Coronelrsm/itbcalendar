package cat.itb.itbcalendar.presentation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;

import cat.itb.itbcalendar.R;
import cat.itb.itbcalendar.presentation.dayofweek.MainFragment;

import static androidx.navigation.Navigation.findNavController;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        NavigationUI.setupActionBarWithNavController(this,findNavController(this, R.id.nav_host_fragment));
    }


    @Override public boolean onSupportNavigateUp() {
        return findNavController(this, R.id.nav_host_fragment).navigateUp();
    }

}

