package cat.itb.itbcalendar.presentation.mpdetail;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cat.itb.itbcalendar.R;
import cat.itb.itbcalendar.presentation.itbcalendar_data_classes.domain.Mp;

public class MP7description extends Fragment {

    private Mp7descriptionViewModel mViewModel;


    TextView name, teacher, description;
    int mpNumber;



    public static MP7description newInstance() {
        return new MP7description();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.mp7description_fragment, container, false);


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        name = view.findViewById(R.id.title);
        teacher = view.findViewById(R.id.teacher);
        description = view.findViewById(R.id.description);
        mpNumber = MP7descriptionArgs.fromBundle(getArguments()).getMpNumber();

    }

    //els continguts del xml, enlloc d'escriure'ls jo, els ha de carregar de la carpeta que ens ha donat el Mateu

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(Mp7descriptionViewModel.class);
        mViewModel.loadMp(mpNumber);
        Mp mp = mViewModel.getMp();

        name.setText(mp.getName());
        teacher.setText(mp.getTeacher());
        description.setText(mp.getDescription());


        // TODO: Use the ViewModel





    }



}
