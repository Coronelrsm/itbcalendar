package cat.itb.itbcalendar.presentation.dayofweek;

import androidx.lifecycle.ViewModel;

import java.util.List;

import cat.itb.itbcalendar.presentation.itbcalendar_data_classes.domain.Mp;
import cat.itb.itbcalendar.presentation.itbcalendar_data_classes.domain.MpSession;
import cat.itb.itbcalendar.presentation.itbcalendar_data_classes.repository.MpRepositoryFactory;

public class MainViewModel extends ViewModel {


    List<MpSession> sessions;

    public void loadSessions() {
        sessions = MpRepositoryFactory.getInstance().getClassesForToday();
    }

    public MpSession getOneSession(){
        return  sessions.get(0);
    }

    public List<MpSession> getAllSessions() {
        return sessions;
    }




}
