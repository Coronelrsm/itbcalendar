package cat.itb.itbcalendar.presentation.dayofweek;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.itbcalendar.R;
import cat.itb.itbcalendar.presentation.itbcalendar_data_classes.domain.MpSession;

public class ModuleAdapter extends RecyclerView.Adapter<ModuleAdapter.MyViewHolder> {

    OnModuleClickedListener onModuleClicked;
    List<MpSession> mpSessionList; //fem que sigui una MpSession enlloc de MP perquè un MpSession conté un Mp un start time i un endtime

    public ModuleAdapter(List<MpSession> mpSessionList) {
        this.mpSessionList = mpSessionList;
    }

    public void setOnModuleClicked(OnModuleClickedListener onModuleClicked) {
        this.onModuleClicked=onModuleClicked;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, teacher, startTime, endTime;

        public MyViewHolder(View view) {
            super(view);
            view.setOnClickListener(this::moduleClicked);
            title =  view.findViewById(R.id.title);
            teacher = view.findViewById(R.id.teacher);
            startTime = view.findViewById(R.id.startTime);
            endTime = view.findViewById(R.id.endTime);
        }

        private void moduleClicked(View view) {
            MpSession mpSession = mpSessionList.get(getAdapterPosition());
            onModuleClicked.onModuleClicked(mpSession);        }
    }



    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.main_fragment_row, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        MpSession mpSession = mpSessionList.get(position);
        holder.title.setText(mpSession.getMp().getName());
        holder.teacher.setText(mpSession.getMp().getTeacher());
        holder.startTime.setText(mpSession.getStartTime());
        holder.endTime.setText(mpSession.getEndTime());




    }

    @Override
    public int getItemCount() {
        return mpSessionList.size();
    }

    public interface OnModuleClickedListener {
        void onModuleClicked(MpSession mpSession);
    }


}
