package cat.itb.itbcalendar.presentation.dayofweek;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

import cat.itb.itbcalendar.R;
import cat.itb.itbcalendar.presentation.itbcalendar_data_classes.domain.MpSession;
import cat.itb.itbcalendar.presentation.mpdetail.MP7description;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel; //el fragment no contacta amb el repositori. Per arquitectura ho ha de fer el view model.

    TextView mpNameView;
    TextView mpDescriptionView;
    View myView;

    private RecyclerView recyclerView;
    private ModuleAdapter mAdapter;

    private RecyclerView.LayoutManager layoutManager;
    private List<MpSession> myData;


    TextView teacher, title, startTime, endTime;


    //aquest main fragment és el que té l'índex de classes


    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //view.setOnClickListener(this::goToDescription);
        //com que ja està a l'adapter, ho comentem perquè el codi no s'apliqui
        //mpNameView = view.findViewById(R.id.mpName); //mpNameView és un EditText o TextView!
        //mpDescriptionView = view.findViewById(R.id.mpDescription);
        //title = view.findViewById(R.id.title);  //els noms haurien de ser iguals, tant Textview com id del xml
        //startTime = view.findViewById(R.id.startTime);
        //endTime = view.findViewById(R.id.endTime);
        //teacher = view.findViewById(R.id.teacher); //el id és el id que hi ha al xml
        recyclerView = view.findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        myView = view;
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);









        //els continguts del xml, enlloc d'escriure'ls jo, els ha de carregar de la carpeta que ens ha donat el Mateu

    }

    private void goToDescription(View view) {
        Navigation.findNavController(view).navigate(R.id.action_mainFragment2_to_MP7description);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        mViewModel.loadSessions();
        myData = mViewModel.getAllSessions();
        mAdapter = new ModuleAdapter(myData);
        mAdapter.setOnModuleClicked(this::navigateToModule);
        recyclerView.setAdapter(mAdapter);


        //title.setText(mpSession.getMp().getName());
        //startTime.setText(mpSession.getStartTime());
        //endTime.setText(mpSession.getEndTime());
        //teacher.setText(mpSession.getMp().getTeacher());

    }

    private void navigateToModule(MpSession mpSession) {

        NavDirections action = MainFragmentDirections.actionMainFragment2ToMP7description(mpSession.getMp().getNumber());
        Navigation.findNavController(myView).navigate(action);

    }

}
