package cat.itb.itbcalendar.presentation.mpdetail;

import androidx.lifecycle.ViewModel;

import cat.itb.itbcalendar.presentation.itbcalendar_data_classes.domain.Mp;
import cat.itb.itbcalendar.presentation.itbcalendar_data_classes.repository.MpRepository;
import cat.itb.itbcalendar.presentation.itbcalendar_data_classes.repository.MpRepositoryFactory;

public class Mp7descriptionViewModel extends ViewModel {
    Mp mp;

    public void loadMp(int mpNumber){
        MpRepository mpRepository = MpRepositoryFactory.getInstance();
        mp = mpRepository.getModuleByNumber(mpNumber);
    }

    public Mp getMp() {
        return mp;
    }
}
