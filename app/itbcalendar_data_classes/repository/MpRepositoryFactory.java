package cat.itb.itbcalendar.repository;

import cat.itb.itbcalendar.repository.data.MpData;
import cat.itb.itbcalendar.repository.data.SessionData;

/**
 * MpRepository Factory
 */
public class MpRepositoryFactory {
    private static MpRepository mpRepository;

    /**
     * @return singleton instance of MpRepository
     */
    public static MpRepository getInstance() {
        if(mpRepository==null){
            mpRepository = new MpRepository(new SessionData(new MpData()));
        }
        return mpRepository;
    }
}
