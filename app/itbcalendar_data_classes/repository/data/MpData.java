package cat.itb.itbcalendar.repository.data;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cat.itb.itbcalendar.domain.Mp;

/**
 * Stores MpData
 */
public class MpData {
    Map<Integer, Mp> modules = new HashMap<>();

    public MpData() {
        createModules();
    }

    private void createModules() {
        addModule(new Mp(7, "Interficies gràfiques", "Mateu Yábar", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."));
        addModule(new Mp(8, "Programació multimèdia i dispositius mòbils", "XXX",  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."));
        addModule(new Mp(9, "Programació de serveis i processos", "XXX",  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."));
        // add more
    }

    private void addModule(Mp mp) {
        modules.put(mp.getNumber(), mp);
    }

    public Mp getMp(int number){
        return modules.get(number);
    }


}
