package cat.itb.itbcalendar.repository.data;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cat.itb.itbcalendar.domain.MpSession;
import cat.itb.itbcalendar.repository.data.MpData;

/**
 * Stores session data
 */
public class SessionData {
    Map<Integer, List<MpSession>> sessions;
    MpData mpData;

    public SessionData(MpData mpData) {
        this.mpData = mpData;
        createData();
    }

    public Map<Integer, List<MpSession>> getSessions() {
        return sessions;
    }

    private void createData(){
        sessions = new HashMap();
        sessions.put(Calendar.MONDAY, createMondayData());
        sessions.put(Calendar.TUESDAY, createTuesdayData());
        sessions.put(Calendar.WEDNESDAY, createWednesdayData());
        sessions.put(Calendar.THURSDAY, createThursdayData());
        sessions.put(Calendar.FRIDAY, createFridayData());
    }

    private List<MpSession> createMondayData() {
        return Arrays.asList(
                new MpSession(mpData.getMp(7), "15:00", "16:00"),
                new MpSession(mpData.getMp(8), "16:00", "17:00"),
                new MpSession(mpData.getMp(9), "17:00", "21:00")
        );
    }

    private List<MpSession> createTuesdayData() {
        return Arrays.asList(
                new MpSession(mpData.getMp(7), "15:00", "16:00"),
                new MpSession(mpData.getMp(8), "16:00", "17:00"),
                new MpSession(mpData.getMp(9), "17:00", "21:00")
        );
    }

    private List<MpSession> createWednesdayData() {
        return Arrays.asList(
                new MpSession(mpData.getMp(7), "15:00", "16:00"),
                new MpSession(mpData.getMp(8), "16:00", "17:00"),
                new MpSession(mpData.getMp(9), "17:00", "21:00")
        );
    }

    private List<MpSession> createThursdayData() {
        return Arrays.asList(
                new MpSession(mpData.getMp(7), "15:00", "16:00"),
                new MpSession(mpData.getMp(8), "16:00", "17:00"),
                new MpSession(mpData.getMp(9), "17:00", "21:00")
        );
    }

    private List<MpSession> createFridayData() {
        return Arrays.asList(
                new MpSession(mpData.getMp(7), "15:00", "16:00"),
                new MpSession(mpData.getMp(8), "16:00", "17:00"),
                new MpSession(mpData.getMp(9), "17:00", "21:00")
        );
    }

    public MpData getMpData() {
        return mpData;
    }
}
