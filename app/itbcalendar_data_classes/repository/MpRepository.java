package cat.itb.itbcalendar.repository;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cat.itb.itbcalendar.domain.Mp;
import cat.itb.itbcalendar.domain.MpSession;
import cat.itb.itbcalendar.repository.data.SessionData;

/**
 * Repository for Session Data.
 */
public class MpRepository {
    private static boolean DEVELOPMENT = true;
    SessionData sessionData;

    public MpRepository(SessionData sessionData) {
        this.sessionData = sessionData;
    }


    /**
     * @return MpsSessions done today
     */
    public List<MpSession> getClassesForToday(){
        int day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        return getClassesForDay(day);
    }


    /**
     * @param mpNumber number of the module
     * @return module with the given number
     */
    public Mp getModuleByNumber(int mpNumber){
        return sessionData.getMpData().getMp(mpNumber);
    }



    public boolean inTime(MpSession mpSession) {
        Calendar start = calendarForHour(mpSession.getStartTime());
        Calendar end = calendarForHour(mpSession.getEndTime());

        DateFormat dateFormat = new SimpleDateFormat("hh:mm");
        String currentHour = dateFormat.format(new Date());
        Date now = calendarForHour(currentHour).getTime();
        return now.after(start.getTime()) && now.before(end.getTime());

    }

    private Calendar calendarForHour(String time) {
        Date timeDate;
        try {
            timeDate = new SimpleDateFormat("HH:mm").parse(time);
        } catch (ParseException e) {
            throw new UnsupportedOperationException(e);
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(timeDate);
        calendar.add(Calendar.DATE, 1);
        return calendar;
    }



    private List<MpSession> getClassesForDay(int dayOfWeek){
        return sessionData.getSessions().get(dayOfWeek);
    }


}
